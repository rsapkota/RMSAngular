import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

const routes: Routes = [
    {
        path: "",
        pathMatch: "full",
        redirectTo: "/dashboard",
    },
    {
        path: "vendors",
        loadChildren: () => import("modules/vendors/vendor-routing.module").then((m) => m.VendorRoutingModule),
    },
    {
        path: "dashboard",
        loadChildren: () => import("modules/dashboard/dashboard-routing.module").then((m) => m.DashboardRoutingModule),
    },
    {
        path: "locationlist",
        loadChildren: () => import("modules/location/location-routing.module").then((m) => m.LocationRoutingModule),
    },
    {
        path: "itemlist",
        loadChildren: () => import("modules/inventory-item/item-routing.module").then((m) => m.ItemRoutingModule),
    },
    {
        path: "emplist",
        loadChildren: () => import("modules/all-employee/allemployee-routing.module").then((m) => m.AllEmployeeRoutingModule),
    },
    {
        path: "auth",
        loadChildren: () => import("modules/auth/auth-routing.module").then((m) => m.AuthRoutingModule),
    },
    {
        path: "error",
        loadChildren: () => import("modules/error/error-routing.module").then((m) => m.ErrorRoutingModule),
    },
    {
        path: "version",
        loadChildren: () => import("modules/utility/utility-routing.module").then((m) => m.UtilityRoutingModule),
    },
    {
        path: "**",
        pathMatch: "full",
        loadChildren: () => import("modules/error/error-routing.module").then((m) => m.ErrorRoutingModule),
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
