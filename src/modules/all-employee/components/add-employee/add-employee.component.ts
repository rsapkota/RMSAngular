import { AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificationService } from '@common/services/notification.service';
import { EmployeeService } from '@modules/all-employee/services/employee.service';
// import fade in animation
import { slideInOutAnimation } from '@modules/_animations/slide-in-out.animation';
import { Observable } from 'rxjs/internal/Observable';
import { finalize } from 'rxjs/operators';
@Component({
    selector: 'sb-add-employee',
    templateUrl: './add-employee.component.html',
    styleUrls: ['./add-employee.component.scss'],
    // make slide in/out animation available to this component
    animations: [slideInOutAnimation],
    // attach the slide in/out animation to the host (root) element of this component
    // tslint:disable-next-line: no-host-metadata-property
    host: { '[@slideInOutAnimation]': '' },
})
export class AddEmployeeComponent implements OnInit, AfterViewInit {
    employeeForm: FormGroup;
    public loading: boolean;
    submitted = false;
    screenHeight: any;
    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private activeRoute: ActivatedRoute,
        private _empSvr: EmployeeService,
        private _notifyService: NotificationService,
        private ref: ChangeDetectorRef
    ) {
        this.loading = false;
        this.initPage();
    }

    ngOnInit(): void {
        this.activeRoute.params.subscribe(routeParams => {
            this.getEmployeeById(routeParams.id);
        });
        this.initEmployeeForm();
    }

    getEmployeeById(id: any) {
        if (id) {
            this._empSvr.getEmployeeById(id).subscribe(res => {
                if (res) {
                    this.employeeForm.patchValue(res);
                }
            })
        }
    }

    initEmployeeForm() {
        this.employeeForm = this.formBuilder.group({
            id: [''],
            firstName: ['', Validators.required],
            middleName: [''],
            lastName: ['', Validators.required],
            dob: [''],
            gender: [''],
            maritalStatus: [''],
            nationality: [''],
            identificationNo: [''],
            temporaryAddress: [''],
            permanentAddress: [''],
            contactNo: [''],
            emergencyContactNumber: [''],
            primaryEmail: ['', Validators.required],
            secondaryEmail: [''],
            designation: [''],
            bloodGroup: [''],
            jobType: [''],
            insurancePolicyNo: [''],
            healthInsurancePolicyNo: [''],
            sSFNo: [''],
            cITNo: [''],
            pFNo: [''],
            panNo: [''],
            fathersName: [''],
            mothersName: [''],
            spouseName: [''],
            photo: [''],
            bankName: [''],
            accountName: [''],
            accountNo: [''],
            bankBranch: [''],
        });
    }

    get f() {
        return this.employeeForm.controls;
    }

    onSubmit() {
        this.submitted = true;
        this.loading = true;
        // stop ere if form is invalid
        if (this.employeeForm.invalid) {
            const invalid = [];
            const controls = this.employeeForm.controls;
            for (const name in controls) {
                if (controls[name].invalid) {
                    invalid.push(name);
                }
            }
            console.log(invalid);
            this.loading = false;
            return;
        }

        let rquestObservable = new Observable<any>();
        if (this.employeeForm.value.id) {
            rquestObservable = this._empSvr
                .employeeUpdate(this.employeeForm.value);
        } else {
            rquestObservable = this._empSvr
                .employeeCreate(this.employeeForm.value);
        }

        // this._empSvr
        //     .employeeCreate(this.employeeForm.value)
        rquestObservable.pipe(
            finalize(() => {
                this.loading = false;
                this.ref.detectChanges();
            })
        )
            .subscribe(
                res => {
                    this._notifyService.showSuccessWithTimeout(
                        'Saved successfully !!',
                        'Notification',
                        3000
                    );
                    this.router.navigate(['/emplist']);
                },
                error => { }
            );
    }

    ngAfterViewInit(): void {
        document.body.style.overflow = 'hidden';
    }

    initPage() {
        const scrnHeight = window.innerHeight;
        this.screenHeight = scrnHeight - (56 + 50);
    }
}