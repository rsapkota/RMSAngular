import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { environment } from "environments/environment";
import { Observable, throwError } from "rxjs";
import { catchError, tap } from "rxjs/operators";

@Injectable()
export class ApiService {
    constructor(private http: HttpClient) {}

    private formatErrors(error: any) {
        return throwError(error.error);
    }

    get(path: string, params: any = {}): Observable<any> {
        return this.http.get(`${environment.api_url}${path}`, { params }).pipe(
            tap((res) => this.getReutnData(res)),
            catchError((err) => this.getformatErrors(err))
        );
    }

    getWithParms(path: string, params: any): Observable<any> {
        return this.http.get(`${environment.api_url}${path}`, { params }).pipe(
            tap((res) => this.getReutnData(res)),
            catchError((err) => this.getformatErrors(err))
        );
    }

    put(path: string, body: object = {}): Observable<any> {
        console.log(body);
        return this.http.put(`${environment.api_url}${path}`, body).pipe(
            tap((res) => this.putReutnData(res)),
            catchError((err) => this.postPutFormatErrors(err))
        );
    }

    private putReutnData(res: any) {
        return res || {};
    }

    post(path: string, body: object = {}): Observable<any> {
        return this.http.post(`${environment.api_url}${path}`, body).pipe(
            tap((res) => this.postReutnData(res)),
            catchError((err) => this.postPutFormatErrors(err))
        );
    }

    delete(path: string): Observable<any> {
        return this.http.delete(`${environment.api_url}${path}`).pipe(
            tap((res) => this.deleteReturnData(res)),
            catchError(this.formatErrors)
        );
    }

    private deleteReturnData(res: any) {
        if (res && res.status && res.message) {
            if (res && res.code === 0) {
                // this.toastr.info('Info', res.message);
            } else {
                // this.toastr.success('Success', res.message);
            }
        }
        return res || {};
    }

    private getReutnData(res: any) {
        return res || {};
    }
    public getformatErrors(error: any): any {
        return throwError(error);
    }

    private postReutnData(res: any) {
        return res || {};
    }

    public postPutFormatErrors(error: any): any {
        return throwError(error);
    }
}
