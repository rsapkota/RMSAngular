/* tslint:disable: ordered-imports*/
import { NgModule } from "@angular/core";
import { CommonModule, DecimalPipe } from "@angular/common";
import { RouterModule } from "@angular/router";

/* Third Party */
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { IconsModule } from "@modules/icons/icons.module";

const thirdParty = [IconsModule, NgbModule];

/* Containers */
import * as appCommonContainers from "./containers";

/* Components */
import * as appCommonComponents from "./components";

/* Guards */
import * as appCommonGuards from "./guards";

/* Services */
import * as appCommonServices from "./services";
import * as authServices from "@modules/auth/services";
import { ApiService } from "./services/api.service";
import { SBSortableHeaderDirective } from './directives/sortable.directive';
import { SortIconComponent } from './components/sort-icon/sort-icon.component';

@NgModule({
    imports: [CommonModule, RouterModule, ...thirdParty],
    providers: [...appCommonServices.services, ...authServices.services, ...appCommonGuards.guards, ApiService],
    declarations: [...appCommonContainers.containers, ...appCommonComponents.components,SBSortableHeaderDirective,SortIconComponent],
    exports: [...appCommonContainers.containers, ...appCommonComponents.components, ...thirdParty,SBSortableHeaderDirective,SortIconComponent],
})
export class AppCommonModule {}
