/* tslint:disable: ordered-imports*/
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SBRouteData } from "@modules/navigation/models";

/* Module */
import { DashboardModule } from "./dashboard.module";

/* Containers */
import * as dashboardContainers from "./containers";

/* Guards */
import * as dashboardGuards from "./guards";
import { AttndDetailsComponent } from "./components/attnd-details/attnd-details.component";

/* Routes */
export const ROUTES: Routes = [
    {
        path: "",
        data: {
            title: "Dashboard - SB Admin Angular",
            breadcrumbs: [
                {
                    text: "Dashboard",
                    active: true,
                },
            ],
        } as SBRouteData,
        canActivate: [],
        component: dashboardContainers.DashboardComponent,
        children: [
            { path: "attdetails/:id", component: AttndDetailsComponent },
            // { path: 'edit/:id', component: ProductAddEditComponent },
        ],
    },
];

@NgModule({
    imports: [DashboardModule, RouterModule.forChild(ROUTES)],
    exports: [RouterModule],
})
export class DashboardRoutingModule {}
