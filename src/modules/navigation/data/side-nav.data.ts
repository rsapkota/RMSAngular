import { SideNavItems, SideNavSection } from "@modules/navigation/models";

export const sideNavSections: SideNavSection[] = [
    {
        text: "CORE",
        items: ["dashboard","vendors"],
    },
    // {
    //     text: 'INTERFACE',
    //     items: ['layouts', 'pages'],
    // },
    // {
    //     text: 'INTERFACE EMPLOYEE',
    //     items: ['employee'],
    // },
    // {
    //     text: 'ADDONS',
    //     items: ['charts', 'tables'],
    // },

    // {
    //     text: "INTERFACE EMPLOYEE",
    //     items: ["allemployee"],
    // },

    {
        text: "INTERFACE LOCATION",
        items: ["alllocation"],
    },
    {
        text: "INTERFACE INVENTORY",
        items: ["itemlist"],
    },
];

export const sideNavItems: SideNavItems = {
    dashboard: {
        icon: "tachometer-alt",
        text: "Dashboard",
        link: "/dashboard",
    },
    vendors: {
        icon: "columns",
        text: "vendors",
        link: "/vendors",
    },
    layouts: {
        icon: "columns",
        text: "Layouts",
        submenu: [
            {
                text: "Static Navigation",
                link: "/dashboard/static",
            },
            {
                text: "Light Sidenav",
                link: "/dashboard/light",
            },
        ],
    },
    // employee: {
    //     icon: "columns",
    //     text: "Employee",
    //     submenu: [
    //         {
    //             text: "Employees",
    //             link: "/emplist",
    //         },
    //     ],
    // },
    location: {
        icon: "columns",
        text: "Location",
        submenu: [
            {
                text: "Location",
                link: "/locationlist",
            },
        ],
    },
    pages: {
        icon: "book-open",
        text: "Pages",
        submenu: [
            {
                text: "Authentication",
                submenu: [
                    {
                        text: "Login",
                        link: "/auth/login",
                    },
                    {
                        text: "Register",
                        link: "/auth/register",
                    },
                    {
                        text: "Forgot Password",
                        link: "/auth/forgot-password",
                    },
                ],
            },
            {
                text: "Error",
                submenu: [
                    {
                        text: "401 Page",
                        link: "/error/401",
                    },
                    {
                        text: "404 Page",
                        link: "/error/404",
                    },
                    {
                        text: "500 Page",
                        link: "/error/500",
                    },
                ],
            },
        ],
    },
    charts: {
        icon: "chart-area",
        text: "Charts",
        link: "/charts",
    },
    tables: {
        icon: "table",
        text: "Tables",
        link: "/tables",
    },
    // allemployee: {
    //     icon: "table",
    //     text: "All Employees",
    //     link: "/emplist",
    // },
    alllocation: {
        icon: "table",
        text: "All Location",
        link: "/locationlist",
    },

    itemlist: {
        icon: "table",
        text: "All Items",
        link: "/itemlist",
    },
    createemployee: {
        icon: "table",
        text: "Create Employee",
        link: "/employee",
    },
};
