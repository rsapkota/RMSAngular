import { DecimalPipe } from '@angular/common';
import { Injectable } from '@angular/core';
import { SortDirection } from '@common/directives/sortable.directive';
import { ApiService } from '@common/services/api.service';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { finalize, switchMap, tap } from 'rxjs/operators';

interface State {
  page: number;
  pageSize: number;
  searchTerm: string;
  sortColumn: string;
  sortDirection: SortDirection;
}


@Injectable({
  providedIn: 'root'
})
export class VendorService {
  private _loading$ = new BehaviorSubject<boolean>(true);
  private _search$ = new Subject<void>();
  private _vendors$ = new BehaviorSubject<any>([]);
  private _total$ = new BehaviorSubject<number>(0);

  private _state: State = {
    page: 1,
    pageSize: 2,
    searchTerm: "",
    sortColumn: "",
    sortDirection: "",
  };

  constructor(private pipe: DecimalPipe, private apiSvr: ApiService) {
    this.callVendorData();
  }

  callVendorData() {
    this.resetState();
    this._search$
      .pipe(
        tap(() => this._loading$.next(true)),
        switchMap(() => this._searchAllVendor()),
        tap(() => this._loading$.next(false))
      )
      .subscribe((result) => {
        if (result && result.data) {
          this._vendors$.next(result.data);
          this._total$.next(result.count);
        } else {
          this._vendors$.next([]);
          this._total$.next(1);
        }
      }, _error => {
        this._vendors$.next([]);
        this._total$.next(1);
        this.resetState();
      });

    this._search$.next();
  }

  resetState() {
    this._state = {
      page: 1,
      pageSize: 5,
      searchTerm: "",
      sortColumn: "",
      sortDirection: "",
    };
  }

  get vendors$() {
    return this._vendors$.asObservable();
  }
  get total$() {
    return this._total$.asObservable();
  }
  get loading$() {
    return this._loading$.asObservable();
  }
  get page() {
    return this._state.page;
  }
  set page(page: number) {
    this._set({ page });
  }
  get pageSize() {
    return this._state.pageSize;
  }
  set pageSize(pageSize: number) {
    this._set({ pageSize });
  }
  get searchTerm() {
    return this._state.searchTerm;
  }
  set searchTerm(searchTerm: string) {
    this._set({ searchTerm });
  }
  set sortColumn(sortColumn: string) {
    this._set({ sortColumn });
  }
  set sortDirection(sortDirection: SortDirection) {
    this._set({ sortDirection });
  }
  private _set(patch: Partial<State>) {
    Object.assign(this._state, patch);
    this._search$.next();
  }
  private _searchAllVendor(): Observable<any> {
    const sendToObj = {
      SearchText: this._state.searchTerm,
      PageSize: this._state.pageSize,
      PageNo: this._state.page,
      OrderByColumn: this._state.sortColumn || 'name',
      OrderByDirection: this._state.sortDirection || 'desc'
    };
    return this.apiSvr.get("/vendor", sendToObj);
  }
  vendorCreate(vndrData: any): Observable<any> {
    return this.apiSvr.post("/Vendor", vndrData);
  }

  vendorUpdate(vndrData: any): Observable<any> {
    return this.apiSvr.put("/Vendor", vndrData).pipe(finalize(() => { this.callVendorData(); }));
  }

  getVendorById(id: any) {
    return this.apiSvr.get(`/Vendor/${id}`);
  }
}