import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SBRouteData } from '@modules/navigation/models';

import { AddVendorComponent } from './add-vendor/add-vendor.component';
import { VendorlistComponent } from './vendorlist/vendorlist.component';
import { VendorsModule } from './vendors.module';


export const ROUTES: Routes = [
  {
      path: "",
      data: {
          title: "Vendor - Create",
          breadcrumbs: [
              {
                  text: "List Vendor",
                  active: true,
              },
          ],
      } as SBRouteData,
      canActivate: [],
      component: VendorlistComponent,
      children: [
          { path: "add", component: AddVendorComponent },
        { path: 'edit/:id', component: AddVendorComponent },
      ],
  },
];


@NgModule({
  declarations: [],
  imports: [
    CommonModule, VendorsModule,
    RouterModule.forChild(ROUTES)
  ],
  exports: [RouterModule],
})

export class VendorRoutingModule
{

 }
