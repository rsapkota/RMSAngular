import { CommonModule, DecimalPipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppCommonModule } from '@common/app-common.module';
import { SortIconComponent } from '@common/components/sort-icon/sort-icon.component';
import { SBSortableHeaderDirective } from '@common/directives/sortable.directive';
import { NavigationModule } from '@modules/navigation/navigation.module';

import { AddVendorComponent } from './add-vendor/add-vendor.component';
import { VendorService } from './services/vendor.service';
import { VendorlistComponent } from './vendorlist/vendorlist.component';


@NgModule({
  providers: [DecimalPipe,VendorService],
  declarations: [VendorlistComponent, AddVendorComponent],
  imports: [CommonModule, RouterModule, ReactiveFormsModule, FormsModule, AppCommonModule, NavigationModule],

})
export class VendorsModule { }
