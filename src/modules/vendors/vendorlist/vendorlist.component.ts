import { ChangeDetectorRef, Component, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { SBSortableHeaderDirective, SortEvent } from '@common/directives';
import { fadeInAnimation } from '@modules/_animations/fade-in.animation';
import { Observable } from 'rxjs';

import { VendorService } from '../services/vendor.service';

@Component({
  selector: 'sb-vendorlist',
  templateUrl: './vendorlist.component.html',
  styleUrls: ['./vendorlist.component.scss'],

  // make fade in animation available to this component
  animations: [fadeInAnimation],
  // attach the fade in animation to the host (root) element of this component
  // tslint:disable-next-line: no-host-metadata-property
  host: { '[@fadeInAnimation]': '' },
})
export class VendorlistComponent implements OnInit {
  // @Input() pageSize = 2;
  pageSize = 5;
  vendors$!: Observable<any>;
  total$!: Observable<number>;
  sortedColumn!: string;
  // tslint:disable-next-line: prettier
  sortedDirection!: string;

  @ViewChildren(SBSortableHeaderDirective) headers!: QueryList<SBSortableHeaderDirective>;

  constructor(
    public vendorService: VendorService,
    private changeDetectorRef: ChangeDetectorRef,
    private router: Router
  ) {
    this.initPage(router);

  }

  ngOnInit(): void {
    this.vendorService.pageSize = this.pageSize;
    this.vendors$ = this.vendorService.vendors$;
    this.total$ = this.vendorService.total$;
  }

  reSetGrid() {
    this.vendorService.callVendorData();
  }

  initPage(router: any) {
    router.events.subscribe(() => {
      document.body.style.overflow = 'visible';
    });
  }

  onSort({ column, direction }: SortEvent) {
    this.sortedColumn = column;
    this.sortedDirection = direction;
    this.vendorService.sortColumn = column;
    this.vendorService.sortDirection = direction;
    this.changeDetectorRef.detectChanges();
  }

}
