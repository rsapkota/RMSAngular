import { Component, OnInit } from '@angular/core';
import { LocationService } from '@modules/location/services/location.service';
import { slideInOutAnimation } from '@modules/_animations/slide-in-out.animation';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'sb-add-location',
  templateUrl: './add-location.component.html',
  styleUrls: ['./add-location.component.scss'],

  // make slide in/out animation available to this component
  animations: [slideInOutAnimation],
  // attach the slide in/out animation to the host (root) element of this component
  // tslint:disable-next-line: no-host-metadata-property
  host: { '[@slideInOutAnimation]': '' },
})
export class AddLocationComponent implements OnInit {
  screenHeight:any;
  loading:any;
  model: any = {};
  constructor(private _locationService: LocationService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.model));

    this._locationService.saveLocation(this.model).pipe(finalize(() => {

    })).subscribe(res => {

    })


  }

}
