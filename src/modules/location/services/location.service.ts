import { Injectable } from '@angular/core';
import { ApiService } from '@common/services/api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private _apiService: ApiService) { }

  saveLocation(data: any): Observable<any> {
    return this._apiService.post('/Location', data);
  }


  getAllLocationTree(): Observable<any> {
    return this._apiService.get('/Location/GetAllLocationFromParent');
  }

}
