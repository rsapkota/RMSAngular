import { CommonModule, DecimalPipe } from '@angular/common';
import { NgModule } from '@angular/core';

import { LocationListComponent } from './location-list/location-list.component';

// tslint:disable-next-line: ordered-imports
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { RouterModule } from '@angular/router';
import { AppCommonModule } from '@common/app-common.module';

import { NavigationModule } from '@modules/navigation/navigation.module';
import { TreeTableModule } from 'ng-treetable';
import { NgxTreeDndModule } from 'ngx-tree-dnd';
import { AddLocationComponent } from './components/add-location/add-location.component';
import { LocationService } from './services/location.service';

@NgModule({
  providers: [DecimalPipe,LocationService],
  declarations: [LocationListComponent, AddLocationComponent],
  imports: [CommonModule, RouterModule,TreeTableModule, ReactiveFormsModule, FormsModule, AppCommonModule, NavigationModule,NgxTreeDndModule],
})
export class LocationModule { }
