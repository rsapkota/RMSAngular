import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { AuthenticationService } from "@modules/auth/services/authentication.service";
import { Observable } from "rxjs";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authenticationService: AuthenticationService) {}
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // add authorization header with jwt token if available
        // const currentUser = this.authenticationService.currentUserValue;
        // const tempToken = 'asdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfsdafafdasdf';
        // if (currentUser && currentUser.token) {
        //     request = request.clone({
        //         setHeaders: {
        //             // Authorization: `Bearer ${currentUser.token}`,
        //             Authorization: `Bearer ${tempToken}`,
        //         },
        //     });
        // }
        // return next.handle(request);

        let headers = request.headers;
        const currentUser = this.authenticationService.currentUserValue;
        const tempToken = "asdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfasdfsdafafdasdf";
        headers = headers.append("Accept", "application/json");
        // if (currentUser && currentUser.token) {
        //     headers = headers.append('Authorization', `Bearer ${tempToken}`);
        // }
        return next.handle(request.clone({ headers }));
    }
}
